package com.sasina.oxprogramunittest;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author admin
 */
public class OXProgramTest {
    
    public OXProgramTest() {
    }

    @Test
    public void testCheckVerticalPlayerOCol1Win() {
        char table[][] = {{'O', '-', '-'},
                                 {'O', '-', '-'},
                                 {'O', '-', '-'}};
        char currentPlayer = 'O';
        int col = 1;
        assertEquals(true, OXProgram.checkVertical(table, currentPlayer, col));
    }
    @Test
    public void testCheckVerticalPlayerOCol2Win() {
        char table[][] = {{'-', 'O', '-'},
                                 {'-', 'O', '-'},
                                 {'-', 'O', '-'}};
        char currentPlayer = 'O';
        int col = 2;
        assertEquals(true, OXProgram.checkVertical(table, currentPlayer, col));
    }
    @Test
    public void testCheckVerticalPlayerOCol3Win() {
        char table[][] = {{'-', '-', 'O'},
                                 {'-', '-', 'O'},
                                 {'-', '-', 'O'}};
        char currentPlayer = 'O';
        int col = 3;
        assertEquals(true, OXProgram.checkVertical(table, currentPlayer, col));
    }

    @Test
    public void testCheckVerticalPlayerOCol1NoWin() {
        char table[][] = {{'O', '-', '-'},
                                 {'O', '-', '-'},
                                 {'-', '-', '-'}};
        char currentPlayer = 'O';
        int col = 1;
        assertEquals(false, OXProgram.checkVertical(table, currentPlayer, col));
    }

    @Test
    public void testCheckVerticalPlayerXCol2Win() {
        char table[][] = {{'-', 'X', '-'},
                                 {'-', 'X', '-'},
                                 {'-', 'X', '-'}};
        char currentPlayer = 'X';
        int col = 2;
        assertEquals(true, OXProgram.checkVertical(table, currentPlayer, col));
    }
    
    @Test
    public void testCheckHorizontalPlayerORow1Win() {
        char table[][] = {{'O', 'O', 'O'},
                                 {'-', '-', '-'},
                                 {'-', '-', '-'}};
        char currentPlayer = 'O';
        int row = 1;
        assertEquals(true, OXProgram.checkHorizontal(table, currentPlayer, row));
    }
    
    @Test
    public void testCheckHorizontalPlayerORow2NoWin() {
        char table[][] = {{'-', '-', '-'},
                                 {'X', 'O', 'O'},
                                 {'-', '-', '-'}};
        char currentPlayer = 'O';
        int row = 2;
        assertEquals(false, OXProgram.checkHorizontal(table, currentPlayer, row));
    }
    
    @Test
    public void testCheckHorizontalPlayerORow2Win() {
        char table[][] = {{'-', '-', '-'},
                                 {'O', 'O', 'O'},
                                 {'-', '-', '-'}};
        char currentPlayer = 'O';
        int row = 2;
        assertEquals(true, OXProgram.checkHorizontal(table, currentPlayer, row));
    }
    
    @Test
    public void testCheckHorizontalPlayerORow3Win() {
        char table[][] = {{'-', '-', '-'},
                                 {'-', '-', '-'},
                                 {'O', 'O', 'O'}};
        char currentPlayer = 'O';
        int row = 3;
        assertEquals(true, OXProgram.checkHorizontal(table, currentPlayer, row));
    }
    
    @Test
    public void testCheckX1PlayerOWin() {
        char table[][] = {{'O', '-', '-'},
                                 {'-', 'O', '-'},
                                 {'-', '-', 'O'}};
        char currentPlayer = 'O';
        assertEquals(true, OXProgram.checkX1(table, currentPlayer));
    }
    
    @Test
    public void testCheckX2PlayerXNoWin() {
        char table[][] = {{'-', '-', 'X'},
                                 {'-', 'X', '-'},
                                 {'O', '-', '-'}};
        char currentPlayer = 'X';
        assertEquals(false, OXProgram.checkX2(table, currentPlayer));
    }
    
    @Test
    public void testCheckX2PlayerXWin() {
        char table[][] = {{'-', '-', 'X'},
                                 {'-', 'X', '-'},
                                 {'X', '-', '-'}};
        char currentPlayer = 'X';
        assertEquals(true, OXProgram.checkX2(table, currentPlayer));
    }
    
    @Test
    public void testCheckDraw() {
        int count = 8;
        assertEquals(true, OXProgram.checkDraw(count));
    }
    
    @Test
    public void testCheckX() {
        char table[][] = {{'O', '-', '-'},
                                 {'-', 'O', '-'},
                                 {'-', '-', 'O'}};
        char currentPlayer = 'O';
        assertEquals(true, OXProgram.checkX(table, currentPlayer));
    }
}
